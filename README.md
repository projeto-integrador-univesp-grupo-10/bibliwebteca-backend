# bibliwebteca

> REST API para bibliwebteca

## About

Este projeto usa o _framework_ [Feathers](http://feathersjs.com).

## Instalação

1.Crie [chaves **ssh** necessárias](https://gitlab.com/-/profile/keys) para baixar os códigos-fontes:

* [Passo a passo geral](https://docs.gitlab.com/ee/user/ssh.html)
* Detalhes no [Windows](https://docs.gitlab.com/ee/user/ssh.html#use-ssh-on-microsoft-windows)

2.Baixe as fontes:

    ```
    $> ssh-add <caminho-em-que-salvou-o-arquivo-*.pub>
    $> git clone git@gitlab.com:projeto-integrador-univesp-grupo-10/bibliwebteca-backend.git
    ```
3. Instale as dependências 

    ```
    $> cd path/to/blibwebteca
    $> yarn install
    ```
4. Crie uma base de dados apropriada para desenvolvimento local, com o usuário `root`:

    ```$> mysql -u root -p
    Digite a senha:
    MariaDB [(none)]> CREATE DATABASE bibliwebteca;
    ```

5. Crie um usuário para a base de dados:

    ``` 
    MariaDB [(none)]> CREATE USER '<meu-usuario>'@'localhost' IDENTIFIED BY '<minha-senha>';
    ```

6. Garanta privilégios da base de dados para o usuário criado:

    ```
    MariaDB [(none)]> GRANT ALL on bibliwebteca.* TO '<meu-usuário>'@localhost ; FLUSH PRIVILEGES;
    ```

7. Saia do modo root:

    ```
    MariaDB [(none)] exit
    $>
    ```

8. Entre com o seu usuário:
    
    ```
    $> mysql -u <meu-usuario> -p
    Digite a senha:
    MariaDB [(none)]
    ```

9. Verifique se a base de dados foi criada:

    ```
    MariaDB [(none)] SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | bibliwebteca       |
    | information_schema |
    | test               |
    +--------------------+
    3 rows in set (0.001 sec)
    ```

10. Crie um arquivo `.env` com as credenciais da base de dados local e outras variáveis:

    ```
    MYSQL_USER=<meu usuario>
    MYSQL_PASSWORD=<minha senha>
    MYSQL_HOST=localhost
    MYSQL_PORT=3306
    MYSQL_DATABASE=bibliwebteca
    AUTH_SECRET=<minha senha de autenticacao>
    AUTH_AUDIENCE=http://localhost:3000
    AUTH_ISSUER=feathers
    PORT=3030
    ADMIN_EMAIL=<meu administrador>
    ADMIN_PASSWORD=<minha senha de administrador>
    ```

Eventualmente, você pode substituir essas credenciais por outras que dão acesso a uma base de dados remota.

## Executando

1. Em modo desenvolvimento:

    ```
    yarn dev
    ```

2. Em modo "normal":

    ```
    yarn start
    ```

## Testando

Execute:

```
$> NODE_ENV=test yarn test
```

 e todos os testes no diretório `test/` serão executados.

 Se quiser observar em modo verborrágico:

 ```
$> yarn test
```

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).
