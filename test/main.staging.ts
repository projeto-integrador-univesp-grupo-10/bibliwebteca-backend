import Mocha from 'mocha';
import { join } from 'path';
import { each } from 'lodash';

const files = [
  'staging/populate.staging.ts'
];

// https://stackoverflow.com/questions/50709059/maxlistenersexceededwarning-possible-eventemitter-memory-leak-detected-11-mess#51166749
import events from 'events';

const LISTENERS = 10;
events.EventEmitter.prototype.setMaxListeners(LISTENERS);
events.defaultMaxListeners = LISTENERS;

process.on('warning', function (err) {
  if ( 'MaxListenersExceededWarning' == err.name ) {
    console.log(err);
    // write to log function
    process.exit(1); // its up to you what then in my case script was hang
  }
});

const mocha = new Mocha({
  timeout: process.env['MOCHA_TIMEOUT'] || 20000,
  checkLeaks: true
});

each(files, async function(f){
  const testfilename = join(__dirname, f);
  mocha.addFile(testfilename);
});

mocha.run(function(failures){
  process.exitCode = failures ? 1 : 0;
});