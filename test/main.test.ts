import Mocha from 'mocha';
import { join } from 'path';
import { each } from 'lodash';

const files = [
  'app.test.ts',
  'services/users.test.ts',
  'services/authentication.test.ts',
  'services/books.test.ts',
  'simulation/admins.create.test.ts',
  'simulation/admins.login.test.ts',
  'simulation/admins.find.test.ts',
  'simulation/users.create.test.ts',
  'simulation/users.login.test.ts',
  'simulation/users.update.test.ts',
  'simulation/users.patch.test.ts',
  'simulation/users.find.test.ts',
  'simulation/books.admin_create.test.ts',
  'simulation/books.admin_find.test.ts',
  'simulation/books.admin_update.test.ts',
  'simulation/books.admin_patch.test.ts',
  'simulation/books.users_create.test.ts',
  'simulation/books.users_find.test.ts',
  'simulation/books.users_update.test.ts',
  'simulation/books.users_patch.test.ts',
  'simulation/books.users_remove.test.ts',
  'simulation/books.admin_remove.test.ts',
  'simulation/users.remove.test.ts',
  'simulation/admins.remove.test.ts'
];

// https://stackoverflow.com/questions/50709059/maxlistenersexceededwarning-possible-eventemitter-memory-leak-detected-11-mess#51166749
import events from 'events';

const LISTENERS = 21;
events.EventEmitter.prototype.setMaxListeners(LISTENERS);
events.defaultMaxListeners = LISTENERS;

process.on('warning', function (err) {
  if ( 'MaxListenersExceededWarning' == err.name ) {
    console.log(err);
    // write to log function
    process.exit(1); // its up to you what then in my case script was hang
  }
});

const mocha = new Mocha({
  timeout: process.env['MOCHA_TIMEOUT'] || 20000,
  checkLeaks: true
});

each(files, async function(f){
  const testfilename = join(__dirname, f);
  mocha.addFile(testfilename);
});

mocha.run(function(failures){
  process.exitCode = failures ? 1 : 0;
});