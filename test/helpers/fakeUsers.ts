import * as _ from 'lodash';
import faker from '@faker-js/faker';
import { User } from './index';

function onRange():User {
  return {
    email: faker.internet.email(),
    password: faker.internet.password(),
    permissions: 'users:get,users:patch,books:find,books:get,books:patch'
  };
}

// eslint-disable-next-line no-unused-vars
export default function(n: number):Array<User>{
  return _.range(n).map(onRange);
}