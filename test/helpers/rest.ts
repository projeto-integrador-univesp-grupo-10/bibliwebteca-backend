import { Options } from './index';

class Rest {

  requester: Chai.ChaiStatic;

  constructor(requester:Chai.ChaiStatic) {
    this.requester = requester;
  }

  execute (
    service: string,
    method: string,
    options: Options
  ):Promise<any> {
    const req = this.requester[method](`/${service}`);
    if (options.accessToken) {
      req.set('Authorization', options.accessToken);
    }
    if (options.data) {
      req.send(options.data);
    }
    return new Promise(function(resolve, reject) {
      req.end(function(err, res){
        if (err) reject(err);
        resolve(res);
      });
    });
  }

  async create(
    service: string,
    options: Options
  ):Promise<any>{
    return await this.execute(service, 'post', options);
  }

  async get(
    service: string,
    options: Options
  ):Promise<any>{
    return await this.execute(service, 'get', options);
  }

  async update(
    service: string,
    options: Options
  ):Promise<any>{
    return await this.execute(service, 'put', options);
  }

  async patch(
    service: string,
    options: Options
  ):Promise<any>{
    return await this.execute(service, 'patch', options);
  }

  async remove(
    service: string,
    options: Options
  ):Promise<any>{
    return await this.execute(service, 'delete', options);
  }

}

export default Rest;
