import Rest from './rest';
import fakeUsers from './fakeUsers';
import fakeBooks from './fakeBooks';

export type Options = {
  accessToken?: string,
  data?: string
}

export type User = {
  id?: string,
  email?: string,
  password?: string,
  accessToken?: string,
  permissions?: string
}

export type Auth = {
  strategy?: string,
  email?: string,
  password?: string
}

export type Book = {
  accessToken?: string,
  userId?: string,
  id?: string,
  title?: string,
  author?: string,
  publishing_company?: string
  year?: number,
  isbn?: string,
  edition?: string,
  pages?: number,
  subject?: string,
  withdrawn_date?: string,
  return_date?: string,
  reservation_date?: string
}

export { Rest, fakeUsers, fakeBooks};