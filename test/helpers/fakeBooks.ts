import * as _ from 'lodash';
import faker from '@faker-js/faker';
import { Book } from './index';

function onRange():Book {
  return {
    title: faker.lorem.words(),
    author: `${faker.name.lastName()}, ${faker.name.firstName()}`,
    publishing_company: faker.company.companyName(),
    isbn: faker.random.numeric(13).toString(),
    edition: `${faker.random.numeric(1)} ${faker.lorem.word()}`,
    year: faker.date.past(30).getFullYear(),
    pages: parseInt(faker.random.numeric(3)),
    subject: faker.lorem.sentence()
  };
}

// eslint-disable-next-line no-unused-vars
export default function(n: number):Array<Book>{
  return _.range(n).map(onRange);
}