// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { admin_credentials  } from '../../src/env_config';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';
import books from './populate.books';


// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// update correct 
admin_credentials(app, process.env);

const admin: User = {
  email: app.get('admin').email,
  password: app.get('admin').password,
  permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
};



describe('Populate staging app', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      done();
    });
  });

  it('should create one admin', async() => {
    try {
      const { body } = await rest.create('users', {
        data: admin
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      body.should.not.have.keys('password');
      body.permissions.should.be.equal('users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove');
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it('should login one admin', async() => {
    try {
      const { body } = await rest.create('authentication', {
        data: {
          strategy: 'local',
          email: admin.email,
          password: admin.password
        }
      });
      body.should.have.any.keys('accessToken');
      admin.accessToken = body.accessToken;
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it.each(books, 'should populate books', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: admin.accessToken,
        data: bookInfo
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'year',
        'isbn',
        'edition',
        'pages',
        'subject'
      );
      next();
    } catch (error) {
      next(error);
    }
  });

});