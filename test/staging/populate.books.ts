import { Book } from '../helpers/index';

const books: Book[] = [
  {
    title: 'Curso de direito tributário',
    author: 'Carvalho, Paulo de Barros',
    publishing_company: 'Saraiva',
    isbn: '9788502060982',
    edition: '18. ed. rev e atual.',
    year: 2007,
    pages: 398,
    subject: 'Direito Tributário, Brasil'
  },
  {
    title: 'Ética profissional e estatuto da advocacia',
    author: 'Macedo Junior, Marco Antonio Silva de',
    publishing_company: 'Saraiva',
    isbn: '9788502086340',
    edition: '2. ed.',
    year: 2010,
    pages: 356,
    subject: 'Ordem do advogados do Brasil, Advogados, Ética profissional, Advocacia, Leis e legislação'
  },
  {
    title: 'Dom Quixote',
    author: 'Harisson, Michel',
    publishing_company: 'Ática',
    isbn: '9788508086948',
    edition: '1. ed.',
    year: 2003,
    pages: 384,
    subject: 'Literatura infantojuvenil'
  },
  {
    title: 'Iracema',
    author: 'Alencar, José de',
    publishing_company: 'L&PM',
    isbn: '9788508122951',
    edition: '1. ed.',
    year: 1997,
    pages: 468,
    subject: 'Ficção brasileira, Literatura brasileira'
  },
  {
    title: 'Os sertões',
    author: 'Jaf, Ivan',
    publishing_company: 'Ática',
    isbn: '9788508127252',
    edition: '1. ed.',
    year: 2009,
    pages: 285,
    subject: 'Cunha, Euclides da,, Literatura infantojuvenil'
  },
  {
    title: 'História antiga',
    author: 'Guarinello, Norberto Luiz',
    publishing_company: 'Contexto',
    isbn: '9788572447942',
    edition: '1. ed.',
    year: 2013,
    pages: 331,
    subject: 'Historia antiga'
  },
  {
    title: 'Direito urbanístico brasileiro',
    author: 'Silva, Jose Afonso da',
    publishing_company: 'Malheiros',
    isbn: '9788574209913',
    edition: '6.ed. rev. e atual.',
    year: 2010,
    pages: 405,
    subject: 'Direito urbanístico'
  },
  {
    title: 'Curso de direito constitucional positivo',
    author: 'Silva, Jose Afonso da',
    publishing_company: 'Malheiros',
    isbn: '9788574209968',
    edition: '33.ed., rev. e atual',
    year: 2010,
    pages: 433,
    subject: 'Direito constitucional'
  },
  {
    title: 'Fundamentos do gerenciamento de serviços de TI',
    author: 'Freitas, Marcos André dos Santos',
    publishing_company: 'Brasport',
    isbn: '9788574525877',
    edition: '2. ed.',
    year: 2013,
    pages: 354,
    subject: 'Informática, Serviço de informação, Administração'
  },
  {
    title: 'Modelagem de processos com BPMN',
    author: 'Campos, André L. N.',
    publishing_company: 'Brasport',
    isbn: '9788574526638',
    edition: '2. ed.',
    year: 2014,
    pages: 491,
    subject: 'Gestão de processos, Modelagem de processos, Reengenharia organizacional'
  },
  {
    title: 'Planejamento tributário',
    author: 'Greco, Marco Aurelio',
    publishing_company: 'Dialética',
    isbn: '9788575002193',
    edition: '3. ed.',
    year: 2011,
    pages: 305,
    subject: 'Direito tributário, Planejamento tributário'
  }
];

export default books;
