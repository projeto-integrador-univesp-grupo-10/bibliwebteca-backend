// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import fakeUsers from '../helpers/fakeUsers';
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const admins: User[] = [];

describe('\'users\' with administration permission create test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'admins', admins);
      done();
    });
  });

  it('should not create admin with wrong permissions', async () => {
    try {
      const user = fakeUsers(1)[0];
      user.permissions = 'service:method';
      const { body } = await rest.create('users', {
        data: user
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.message.should.be.equal('Validation error: Permission service:method not valid');
      expect(body.errors).to.have.length(1);
      body.errors[0].message.should.be.equal('Permission service:method not valid');
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it('should create one admin', async() => {
    try {
      const user = fakeUsers(1)[0];
      user.permissions = 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove';
      const { body } = await rest.create('users', {
        data: user
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      body.should.not.have.keys('password');
      body.permissions.should.be.equal('users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove');
      admins.push({
        email: user.email,
        password: user.password,
        id: body.id
      });
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });
  
  it('should not create one admin due to forbidden permissions', async() => {
    try {
      const user = fakeUsers(1)[0];
      user.permissions = 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove';
      const { body } = await rest.create('users', {
        data: user
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.message.should.be.equal('User cannot be created');
      expect(body.data).to.have.length(9);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it('should create a guest that will be patched as a second admin', async() => {
    try {
      const user = fakeUsers(1)[0];
      const adminPermissions = 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove';
      
      const guest = await rest.create('users', {
        data: user
      });

      guest.body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      guest.body.should.not.have.keys('password');
      guest.body.permissions.should.be.not.equal(adminPermissions);
      

      // now login as admin
      const admin = await rest.create('authentication', {
        data: {
          strategy: 'local',
          email: admins[0].email,
          password: admins[0].password
        }
      });

      // and patch guest as a new admin
      const newAdmin = await rest.patch(`users/${guest.body.id}`, {
        accessToken: admin.body.accessToken,
        data: {
          permissions: adminPermissions
        }
      });
      
      newAdmin.body.permissions.should.be.equal(adminPermissions);
      admins.push({
        email: user.email,
        password: user.password,
        id: newAdmin.body.id
      });
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it('should create a guest that will be patched -- by the second admin -- as a third admin', async() => {
    try {
      const user = fakeUsers(1)[0];
      const adminPermissions = 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove';
      
      const guest = await rest.create('users', {
        data: user
      });

      guest.body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      guest.body.should.not.have.keys('password');
      guest.body.permissions.should.be.not.equal(adminPermissions);
      

      // now login as admin
      const admin = await rest.create('authentication', {
        data: {
          strategy: 'local',
          email: admins[1].email,
          password: admins[1].password
        }
      });

      // and patch guest as a new admin
      const newAdmin = await rest.patch(`users/${guest.body.id}`, {
        accessToken: admin.body.accessToken,
        data: {
          permissions: adminPermissions
        }
      });

      admins.push({
        email: user.email,
        password: user.password,
        id: newAdmin.body.id
      });
      newAdmin.body.permissions.should.be.equal(adminPermissions);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  
});