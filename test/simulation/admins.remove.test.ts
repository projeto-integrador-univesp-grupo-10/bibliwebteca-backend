// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const users: User[] = [];
const admins: User[] = [];

describe('\'users\' with administration permission remove test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const usersDB = await rs.get('db', 'users');
      const adminsDB = await rs.get('db', 'admins');
      each(usersDB.result[0], function(u) {
        users.push(u);
      });
      each(adminsDB.result[0], function(u) {
        admins.push(u);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'users', users);
      done();
    });
  });

  it('first admin should remove first user', async () => {
    try {
      const { body } = await rest.remove(`users/${users[0].id}`, {
        accessToken: admins[0].accessToken
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('second admin should remove second user', async () => {
    try {
      const { body } = await rest.remove(`users/${users[1].id}`, {
        accessToken: admins[1].accessToken
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('third admin should remove third user', async () => {
    try {
      const { body } = await rest.remove(`users/${users[2].id}`, {
        accessToken: admins[2].accessToken
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it.each(admins, 'should remove itself', async (adminInfo, next) => {
    try {
      const { body } = await rest.remove(`users/${adminInfo.id}`, {
        accessToken: adminInfo.accessToken
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      next();
    } catch (error) {
      next(error);
    }
  });

});