// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import fakeUsers from '../helpers/fakeUsers';
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const users: User[] = fakeUsers(3);

describe('\'users\' with guest permission create test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'users', users);
      done();
    });
  });

  it('should not create user with wrong permissions', async () => {
    try {
      const user = fakeUsers(1)[0];
      user.permissions = 'service:method';
      const { body } = await rest.create('users', {
        data: user
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.message.should.be.equal('Validation error: Permission service:method not valid');
      expect(body.errors).to.have.length(1);
      body.errors[0].message.should.be.equal('Permission service:method not valid');
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it.each(users, 'should create users', async (userInfo, next) => {
    try {
      const { body } = await rest.create('users', {
        data: userInfo
      });

      body.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      body.should.not.have.keys('password');
      body.permissions.should.be.equal('users:get,users:patch,books:find,books:get,books:patch');

      // Save it for future edition
      userInfo.id = body.id;
      next();
    } catch (error) {
      next(error);
    }
  });

});