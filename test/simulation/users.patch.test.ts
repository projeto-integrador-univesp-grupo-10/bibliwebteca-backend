// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';
import { each } from 'lodash';

//local libraries
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const users: User[] = [];

describe('\'users\' with guest permission patch test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const usersDB = await rs.get('db', 'users');
      each(usersDB.result[0], function(u) {
        users.push(u);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'users', users);
      done();
    });
  });

  it.each(users, 'should deny patch own permissions to administrator capabilities', async (userInfo, next)  => {
    try {
      const { body } = await rest.patch(`users/${userInfo.id}`, {
        accessToken: userInfo.accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${userInfo.id} cannot change permissions`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it('should deny first user patch second user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[1].id}`, {
        accessToken: users[0].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[0].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('should deny first user patch third user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[2].id}`, {
        accessToken: users[0].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[0].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('should deny second user patch first user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[0].id}`, {
        accessToken: users[1].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[0].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('should deny second user patch third user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[0].id}`, {
        accessToken: users[1].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[1].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('should deny third user patch first user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[0].id}`, {
        accessToken: users[2].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[2].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

  it('should deny thrid user patch second user\'s permissions to administrator capabilities', async ()  => {
    try {
      const { body } = await rest.patch(`users/${users[1].id}`, {
        accessToken: users[2].accessToken,
        data: {
          permissions: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${users[2].id} cannot change permissions`);
      Promise.resolve();
    } catch (error) {
      Promise.reject(error);
    }
  });

});