// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with administration permission patch test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'books', books);
      done();
    });
  });


  it.each(books, 'should not reserve book due forbidden permission', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot patch`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not withdrawn book due forbidden permission', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot patch`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not return book due forbidden permission', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot patch`);
      next();
    } catch (error) {
      next(error);
    }
  });
});