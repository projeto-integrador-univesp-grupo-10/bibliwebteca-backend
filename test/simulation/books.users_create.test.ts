// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, User, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});


const books: Book[] = [];

describe('\'users\' with guest permission create test', () => {

  let server;
  let requester;
  let rest;
  

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const usersDB = await rs.get('db', 'users');
      const users: User[] = usersDB.result[0];
      each(users, function(u) {
        const __books__ = fakeBooks(3);
        each(__books__, function(b) {
          b.accessToken = u.accessToken;
          b.userId = u.id;
          books.push(b);
        });
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      done();
    });
  });

  it.each(books, 'should not create book with empty data due forbidden permissions', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot create`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book due forbidden permissions', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot create`);
      next();
    } catch (error) {
      next(error);
    }
  });
});