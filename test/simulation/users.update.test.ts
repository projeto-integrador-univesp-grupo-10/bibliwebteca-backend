// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';
import { each } from 'lodash';

//local libraries
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const users: User[] = [];

describe('\'users\' with guest permission update test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const usersDB = await rs.get('db', 'users');
      each(usersDB.result[0], function(u) {
        users.push(u);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'users', users);
      done();
    });
  });

  it.each(users, 'should deny update user', async (userInfo, next)  => {
    try {
      const { body } = await rest.update(`users/${userInfo.id}`, {
        accessToken: userInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('MethodNotAllowed');
      body.code.should.be.equal(405);
      body.message.should.be.equal('Provider \'rest\' can not call \'update\'. (disallow)');
      next();
    } catch (error) {
      next(error);
    }
  });

});