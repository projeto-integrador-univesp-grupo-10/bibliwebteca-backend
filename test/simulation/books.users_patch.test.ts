// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';
import { stringify } from 'qs';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with guest permission patch test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'users_books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      done();
    });
  });      

  it.each(books, 'should not patch title\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          title: newBook.title,
 
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field title may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch author\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          author: newBook.author
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field author may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch publishing_company\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          publishing_company: newBook.publishing_company
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field publishing_company may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch isbn\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          isbn: newBook.isbn
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field isbn may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch edition\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          edition: newBook.edition
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field edition may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch pages\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          pages: newBook.pages
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field pages may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not patch subject\'s book', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          subject: newBook.subject
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field subject may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not reserve, withdrawn or reserve book (patch userId and reservation_date) due to empty data', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Patch needs an action. Use \'reserve\', \'withdrawn\' or \'return\'');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not withdrawn book (patch withdrawn_date) due to a non previous reservation', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} cannot be withdrawn`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not return book (patch userId and return_date) due to a non previous reservation', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'return'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} cannot be returned`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should reserve book (patch userId and reservation_date)', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'withdrawn_date',
        'return_date',
        'reservation_date',
        'userId'
      );
      expect(body.userId).match(/[a-f0-9]+\-[a-f0-9]+\-[a-f0-9]+\-[a-f0-9]+/g);
      body.userId.should.be.equal(bookInfo.userId);
      body.reservation_date.should.be.a('string');
      const reservation_date = new Date(body.reservation_date);
      reservation_date.should.be.a.instanceOf(Date);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find books with some state by non null userId', async (bookInfo, next) => {
    try {
      const query = stringify({
        userId: {
          $ne: 'null'
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find reserved books by userId and reservation_date not equals null', async (bookInfo, next) => {
    try {
      const query = stringify({
        userId: {
          $ne: 'null'
        },
        reservation_date: {
          $ne: 'null'
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not reserve book (patch userId and reservation_date) due to previous reservation', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} is reserved`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should withdrawn book (patch withdrawn_date)', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'withdrawn_date',
        'return_date',
        'reservation_date',
        'userId'
      );
      expect(body.userId).match(/[a-f0-9]+\-[a-f0-9]+\-[a-f0-9]+\-[a-f0-9]+/g);
      body.userId.should.be.equal(bookInfo.userId);
      body.withdrawn_date.should.be.a('string');
      const withdrawn_date = new Date(body.withdrawn_date);
      withdrawn_date.should.be.a.instanceOf(Date);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find withdrawned books by userId, withdrawn_date and return_date not equals null', async (bookInfo, next) => {
    try {
      const query = stringify({
        userId: {
          $ne: 'null'
        },
        withdrawn_date: {
          $ne: 'null'
        },
        return_date: {
          $ne: 'null'
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not withdrawn book (patch withdrawn_date) due to previous withdrawn', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} cannot be withdrawn`);
      body.errors[0].should.be.equal(`User ${bookInfo.userId} already withdrawned book ${bookInfo.id}`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should return book (patch withdrawn_date)', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'return'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'withdrawn_date',
        'return_date',
        'reservation_date',
        'userId'
      );
      expect(body.userId).to.be.null;
      expect(body.reservation_date).to.be.null;
      expect(body.withdrawn_date).to.be.null;
      expect(body.return_date).to.be.null;
      next();
    } catch (error) {
      next(error);
    }
  });
});