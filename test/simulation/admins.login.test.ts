// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';
import { each } from 'lodash';

//local libraries
import app from '../../src/app';
import { User, fakeUsers } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const admins: User[] = [];

describe('\'users\' with administration permission login test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const adminsDB = await rs.get('db', 'admins');
      each(adminsDB.result[0], function(a) {
        admins.push(a);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'admins', admins);
      done();
    });
  });

  it.each(admins, 'should deny login for wrong admin', async (adminInfo, next)  => {
    try {
      const falseUserInfo: User = {
        email: fakeUsers(1)[0].email,
        password: fakeUsers(1)[0].password
      };
      const { body } = await rest.create('authentication', {
        data: {
          strategy: 'local',
          ...falseUserInfo
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('NotAuthenticated');
      body.code.should.be.equal(401);
      body.message.should.be.equal('Invalid login');
      next();
    } catch (error) {
      next(error);
    }
  });


  it.each(admins, 'should deny login for admins due wrong password', async (adminInfo, next)  => {
    try {
      const falseUserInfo: User = { email: adminInfo.email, password: 'password' };
      const { body } = await rest.create('authentication', {
        data: {
          strategy: 'local',
          ...falseUserInfo
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('NotAuthenticated');
      body.code.should.be.equal(401);
      body.message.should.be.equal('Invalid login');
      next();
    } catch (error) {
      next(error);
    }
  });


  it.each(admins, 'should login admins for correct password', async (adminInfo, next)  => {
    try {
      const { body } = await rest.create('authentication', {
        data: {
          strategy: 'local',
          ...adminInfo
        }
      });
      
      body.should.have.any.keys(
        'accessToken',
        'authentication',
        'user'
      );

      // check keys
      body.authentication.should.have.any.keys(
        'strategy',
        'accessToken',
        'payload'
      );

      // check types of values
      body.accessToken.should.be.a('string');
      body.authentication.should.be.a('object');
      body.user.should.be.a('object');

      body.authentication.payload.should.have.any.keys(
        'iat',
        'exp',
        'aud',
        'iss',
        'sub',
        'jti'
      );
      body.user.should.have.any.keys(
        'id',
        'email',
        'permissions',
        'createdAt',
        'updatedAt'
      );
      body.user.should.not.have.any.keys('password');
      body.user.email.should.be.equal(adminInfo.email);
      body.user.permissions.should.be.equal('users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove');

      // save information to future access on tests
      adminInfo.accessToken = body.accessToken;
      next();
    } catch (error) {
      next(error);
    }
  });
});