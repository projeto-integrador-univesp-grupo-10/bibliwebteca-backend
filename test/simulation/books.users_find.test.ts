// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book, User } from '../helpers';
import { each } from 'lodash';
import { stringify } from 'qs';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with guest permission find test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const usersDB = await rs.get('db', 'users');
      const bookDB = await rs.get('db', 'users_books');
      
      const users: User[] = usersDB.result[0];
      const _books: Book[] = bookDB.result[0];

      each(users, function(u, i) {
        each(_books, function(b, j) {
          if (
            i === 0 && j <= 2 ||
            i === 1 && (j > 2 && j<= 5) ||
            i === 2 && j > 5
          ) {
            b.accessToken = u.accessToken;
            b.userId = u.id;
            books.push(b);
          }
        });
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'users_books', books);
      done();
    });
  });

  it.each(books, 'should get a list of all books', async (bookInfo, next) => {
    try {
      const { body } = await rest.get('books', {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      body.total.should.be.equal(books.length);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should get a list with one book', async (bookInfo, next) => {
    try {
      const query = stringify({
        $limit: 1,
        id: {
          $eq: bookInfo.id
        }
      });

      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.any.keys(
        'data',
        'limit',
        'skip',
        'total'
      );
      body.should.have.property('total');
      body.total.should.be.equal(1);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should get a list with all books except one', async (bookInfo, next) => {
    try {
      const query = stringify({
        id: {
          $ne: bookInfo.id
        }
      });

      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.any.keys(
        'data',
        'limit',
        'skip',
        'total'
      );
      body.should.have.property('total');
      body.total.should.be.equal(books.length - 1);
      next();
    } catch (error) {
      next(error);
    }
  });
  
});