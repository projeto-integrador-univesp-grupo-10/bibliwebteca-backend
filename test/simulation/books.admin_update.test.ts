// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with administration permission update test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'books', books);
      
      // copy data, with some modifications
      // to be used in books.users_<find | update | patch>.test.ts
      each(books, function(b) {
        delete b.accessToken;
        delete b.userId;
      });
      await rs.post('db', 'users_books', books);
      done();
    });
  });


  it.each(books, 'should not update book with empty data', async (bookInfo, next) => {
    try {
      const { body } = await rest.update(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['title', 'author', 'publishing_company', 'year', 'isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should update book ', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.update(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          title: newBook.title,
          author: newBook.author,
          publishing_company: newBook.publishing_company,
          year: newBook.year,
          isbn: newBook.isbn,
          edition: newBook.edition, 
          pages: newBook.pages,
          subject: newBook.subject 
        }
      });

      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'year',
        'isbn',
        'edition',
        'pages',
        'subject',
        'createdAt',
        'updatedAt',
        'userId',
        'reservation_date'
      );

      body.id.should.be.equal(bookInfo.id);
      body.title.should.be.not.equal(bookInfo.title);
      body.author.should.be.not.equal(bookInfo.author);
      body.publishing_company.should.be.not.equal(bookInfo.publishing_company);
      body.year.should.be.not.equal(bookInfo.year);
      body.isbn.should.be.not.equal(bookInfo.isbn);
      body.edition.should.be.not.equal(bookInfo.edition);
      body.pages.should.be.not.equal(bookInfo.pages);
      body.subject.should.be.not.equal(bookInfo.subject);

      body.title.should.be.equal(newBook.title);
      body.author.should.be.equal(newBook.author);
      body.publishing_company.should.be.equal(newBook.publishing_company);
      body.year.should.be.equal(newBook.year);
      body.isbn.should.be.equal(newBook.isbn);
      body.edition.should.be.equal(newBook.edition);
      body.pages.should.be.equal(newBook.pages);
      body.subject.should.be.equal(newBook.subject);

      expect(body.reservation_date).to.be.null;
      expect(body.withdrawn_date).to.be.null;
      expect(body.return_date).to.be.null;

      bookInfo.id = body.id;
      next();
    } catch (error) {
      next(error);
    }
  });
});