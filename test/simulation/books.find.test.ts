// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book } from '../helpers';
import { each } from 'lodash';
import { stringify } from 'qs';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'books\' found by API request', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      done();
    });
  });


  it.each(books, 'should find all books', async (bookInfo, next) => {
    try {
      const { body } = await rest.get('books', {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      body.total.should.be.equal(books.length);
      next();
    } catch (error) {
      next(error);
    }
  });
  it.each(books, 'should find all books different of itself ', async (bookInfo, next) => {
    try {
      const query = stringify({
        id: {
          $ne: bookInfo.id
        }
      });

      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      body.total.should.be.equal(books.length - 1);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should get itself ', async (bookInfo, next) => {
    try {
      const { body } = await rest.get(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'withdrawn_date',
        'return_date',
        'reservation_date'
      );
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find only itself ', async (bookInfo, next) => {
    try {
      const query = stringify({
        $limit: 1, 
        id: {
          $eq: bookInfo.id
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      body.total.should.be.equal(1);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find titles by first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        title: {
          $like: `${bookInfo.title[0]}%`
        }
      });
    
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find titles by a letter different of first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        title: {
          $notLike: `${bookInfo.title[0]}%`
        }
      });
    
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find authors by first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        author: {
          $like: `${bookInfo.author[0]}%`
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find authors by a letter different of first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        author: {
          $notLike: `${bookInfo.author[0]}%`
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find publishing_company by first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        publishing_company: {
          $like: `${bookInfo.publishing_company[0]}%`
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find publishing_company by a letter different of first letter (sensitive case)', async (bookInfo, next) => {
    try {
      const query = stringify({
        publishing_company: {
          $notLike: `${bookInfo.publishing_company[0]}%`
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should find isbn by random number', async (bookInfo, next) => {
    try {
      const query = stringify({
        isbn: {
          $like: `%${Math.floor(Math.random() * 10 )}%`
        }
      });
      const { body } = await rest.get(`books?${query}`, {
        accessToken: bookInfo.accessToken
      });

      body.should.have.property('total');
      expect(body.total).greaterThan(0);
      next();
    } catch (error) {
      next(error);
    }
  });

});