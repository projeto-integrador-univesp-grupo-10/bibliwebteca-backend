// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with administration permission remove test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      
      each(books, function(b, i) {
        delete books[i];
      });
      await rs.post('db', 'books', books);

      const bookDB = await rs.get('db', 'users_books');
      const users_books = bookDB.result[0];
      each(users_books, function(b, i) {
        delete users_books[i];
      });
      done();
    });
  });


  it.each(books, 'should remove book', async (bookInfo, next) => {
    try {
      let { body } = await rest.remove(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken
      });

      // Now check if book dos not exist anymore
      const res = await rest.get(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken
      });
      body = res.body;
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('NotFound');
      body.code.should.be.equal(404);
      body.message.should.be.equal(`No record found for id '${bookInfo.id}'`);
      next();
    } catch (error) {
      next(error);
    }
  });

});