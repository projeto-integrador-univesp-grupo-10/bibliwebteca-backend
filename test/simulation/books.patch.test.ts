// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'books\' patched by API request', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'books', books);
      await rs.post('db', 'users_books', books);
      done();
    });
  });

  it.each(books, 'should not be able to change book title', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          title: fakeBooks(1)[0].title
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field title may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book author', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          author: fakeBooks(1)[0].author
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field author may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book publishing_company', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          publishing_company: fakeBooks(1)[0].publishing_company
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field publishing_company may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book isbn', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          isbn: fakeBooks(1)[0].isbn
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field isbn may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book edition', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          edition: fakeBooks(1)[0].edition
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field edition may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book pages', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          pages: fakeBooks(1)[0].pages
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field pages may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to change book subject', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          subject: fakeBooks(1)[0].subject
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field subject may not be patched. (preventChanges)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to reserve (patch without \'reserve\' action)', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Patch needs an action. Use \'reserve\', \'withdrawn\' or \'return\'');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to reserve (patch with wrong action)', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'anyother'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Patch needs a valid action. Use \'reserve\', \'withdrawn\' or \'return\'');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should be able to reserve', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'createdAt',
        'updatedAt',
        'userId',
        'reservation_date'
      );
      body.userId.should.be.equal(bookInfo.userId);
      expect(body.reservation_date).match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to reserve due to previous reservation', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} is reserved`);
      expect(body.errors).has.length(1);
      body.errors[0].should.be.equal(`User ${bookInfo.userId} already reserved book ${bookInfo.id}`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it('should not be able to reserve due to another user that belongs it', async () => {
    try {
      const { body } = await rest.patch(`books/${books[0].id}`, {
        accessToken: books[3].accessToken,
        data: {
          action: 'reserve'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${books[0].id} is reserved`);
      expect(body.errors).has.length(2);
      body.errors[0].should.be.equal(`User ${books[3].userId} do not belongs the reserved book ${books[0].id}`);
      body.errors[1].should.be.equal(`Book ${books[0].id} belongs to user ${books[0].userId}`);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it('should not be able to withdrawn due to another user that belongs it', async () => {
    try {
      const { body } = await rest.patch(`books/${books[0].id}`, {
        accessToken: books[3].accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${books[0].id} cannot be withdrawn`);
      expect(body.errors).has.length(2);
      body.errors[0].should.be.equal(`User ${books[3].userId} do not belongs book ${books[0].id}`);
      body.errors[1].should.be.equal(`Book ${books[0].id} belongs to user ${books[0].userId}`);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it.each(books, 'should be able to withdrawn ', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'createdAt',
        'updatedAt',
        'userId',
        'reservation_date'
      );
      body.userId.should.be.equal(bookInfo.userId);
      expect(body.reservation_date).to.be.null;
      expect(body.withdrawn_date).match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
      expect(body.return_date).match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to withdrawn due previous withdrawn ', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'withdrawn'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} cannot be withdrawn`);
      expect(body.errors).has.length(1);
      body.errors[0].should.be.equal(`User ${bookInfo.userId} already withdrawned book ${bookInfo.id}`);
      next();
    } catch (error) {
      next(error);
    }
  });

  it('should not be able to return due to another user that belongs it', async () => {
    try {
      const { body } = await rest.patch(`books/${books[0].id}`, {
        accessToken: books[3].accessToken,
        data: {
          action: 'return'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${books[0].id} cannot be returned`);
      expect(body.errors).has.length(2);
      body.errors[0].should.be.equal(`User ${books[3].userId} do not belongs book ${books[0].id}`);
      body.errors[1].should.be.equal(`Book ${books[0].id} belongs to user ${books[0].userId}`);
      return Promise.resolve(true);
    } catch (error) {
      return Promise.reject(error);
    }
  });

  it.each(books, 'should be able to return', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'return'
        }
      });
      body.should.have.any.keys(
        'id',
        'title',
        'author',
        'publishing_company',
        'isbn',
        'edition',
        'pages',
        'subject',
        'createdAt',
        'updatedAt',
        'userId',
        'reservation_date'
      );
      expect(body.userId).to.be.null;
      expect(body.reservation_date).to.be.null;
      expect(body.withdrawn_date).to.be.null;
      expect(body.return_date).to.be.null;
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not be able to return due to previous return', async (bookInfo, next) => {
    try {
      const { body } = await rest.patch(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          action: 'return'
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal(`Book ${bookInfo.id} cannot be returned`);
      expect(body.errors).has.length(1);
      body.errors[0].should.be.equal(`Book ${bookInfo.id} already returned`);
      next();
    } catch (error) {
      next(error);
    }
  });
  
});