// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';
import dayjs from 'dayjs';

//local libraries
import app from '../../src/app';
import { Rest, User, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});


const books: Book[] = [];

let book_index = 0;

describe('\'users\' with administration permission create books test', () => {

  let server;
  let requester;
  let rest;
  

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const adminsDB = await rs.get('db', 'admins');
      const admins: User[] = adminsDB.result[0];
      each(admins, function(a) {
        const __books__ = fakeBooks(3);
        each(__books__, function(b) {
          b.accessToken = a.accessToken;
          b.userId = a.id;
          books.push(b);
        });
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'books', books);
      done();
    });
  });

  it.each(books, 'should not create book without empty data', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {}
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['title', 'author', 'publishing_company', 'year', 'isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['author', 'publishing_company', 'year', 'isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title and author', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['publishing_company', 'year', 'isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title, author and publishing_company', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['year', 'isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title, author, publishing_company and year', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['isbn', 'edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title, author, publishing_company, year and isbn', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['edition', 'pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title, author, publishing_company, year, isbn and edition', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['pages', 'subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with only title, author, publishing_company, year, isbn, edition and pages', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      each(['subject'], function(param, i){
        body.errors[i].message.should.be.equal(`books.${param} cannot be null`);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with title, author, publishing_company, yearm isbn, edition, pages, subject and userId', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject,
          userId: bookInfo.userId
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field userId may not be created. (preventCreation)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with title, author, publishing_company, year, isbn, edition, pages, subject and reservation_date', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject,
          reservation_date: dayjs()
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field reservation_date may not be created. (preventCreation)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with title, author, publishing_company, year, isbn, edition, pages, subject and withdrawn_date', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject,
          withdrawn_date: dayjs()
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field withdrawn_date may not be created. (preventCreation)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should not create book with title, author, publishing_company, year, isbn, edition, pages, subject and return_date', async (bookInfo, next) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject,
          return_date: dayjs()
        }
      });
      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('BadRequest');
      body.code.should.be.equal(400);
      body.message.should.be.equal('Field return_date may not be created. (preventCreation)');
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(books, 'should create book with title, author, publishing_company, year, isbn, edition, pages and subject', async (bookInfo: any, next: any) => {
    try {
      const { body } = await rest.create('books', {
        accessToken: bookInfo.accessToken,
        data: {
          title: bookInfo.title,
          author: bookInfo.author,
          publishing_company: bookInfo.publishing_company,
          year: bookInfo.year,
          isbn: bookInfo.isbn,
          edition: bookInfo.edition,
          pages: bookInfo.pages,
          subject: bookInfo.subject
        }
      });
      
      // Not empty data
      body.id.should.be.a('string');
      body.title.should.be.a('string');
      body.author.should.be.a('string');
      body.publishing_company.should.be.a('string');
      body.year.should.be.a('number');
      body.isbn.should.be.a('string');
      body.edition.should.be.a('string');
      body.pages.should.be.a('number');
      body.subject.should.be.a('string');
      body.createdAt.should.be.a('string');
      body.updatedAt.should.be.a('string');

      // Empty data
      expect(body.userId).to.be.undefined;
      expect(body.withdrawn_date).to.be.undefined;
      expect(body.return_date).to.be.undefined;
      expect(body.reservation_date).to.be.undefined;

      // Save id to future edition
      books[book_index].id = body.id;
      book_index += 1;
      next();
    } catch (error) {
      next(error);
    }
  });
});