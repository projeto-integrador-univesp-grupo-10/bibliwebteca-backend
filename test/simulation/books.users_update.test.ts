// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';

//local libraries
import app from '../../src/app';
import { Rest, Book, fakeBooks } from '../helpers';
import { each } from 'lodash';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

const books: Book[] = [];

describe('\'users\' with guest permission update test', () => { 

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const bookDB = await rs.get('db', 'users_books');
      each(bookDB.result[0], function(b) {
        books.push(b);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      done();
    });
  });

  it.each(books, 'should not update book due forbidden permission', async (bookInfo, next) => {
    try {
      const newBook = fakeBooks(1)[0];
      const { body } = await rest.update(`books/${bookInfo.id}`, {
        accessToken: bookInfo.accessToken,
        data: {
          title: newBook.title,
          author: newBook.author,
          publishing_company: newBook.publishing_company,
          isbn: newBook.isbn,
          edition: newBook.edition, 
          pages: newBook.pages,
          subject: newBook.subject 
        }
      });

      body.should.have.any.keys(
        'name',
        'message',
        'code',
        'className',
        'data',
        'errors'
      );
      body.name.should.be.equal('Forbidden');
      body.code.should.be.equal(403);
      body.message.should.be.equal(`User ${bookInfo.userId} cannot update`);
      next();
    } catch (error) {
      next(error);
    }
  });
});