// main libraries
import { join } from 'path';

// third party libraries
import it_each from 'it-each';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import rs from 'rocket-store';
import { stringify } from 'qs';
import { each, find } from 'lodash';

//local libraries
import app from '../../src/app';
import { User } from '../helpers/index';
import Rest from '../helpers/rest';

// loads iteration in it functions
it_each();

// Configure chai test functions
chai.use(chaiHttp);
chai.should();

// Create a mocked database to feed real database// configure chai
rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
});

// load
const admins: User[] = [];

describe('\'users\' with administration permission find test', () => {

  let server;
  let requester;
  let rest;

  // load server
  // and requester
  before(function(done) {
    const port = app.get('port') || 8998;
    server = app.listen(port);
    server.once('listening', async function() {
      requester = chai.request(server).keepOpen();
      rest = new Rest(requester);

      // load mocked database
      const adminsDB = await rs.get('db', 'admins');
      each(adminsDB.result[0], function(a) {
        admins.push(a);
      });
      done();
    });
  });

  // After all test be ok
  // load users on a mocked database
  // to reuse them in another tests
  after(function(done){
    server.close(async function() {
      requester.close();
      await rs.post('db', 'admins', admins);
      done();
    });
  });


  it.each(admins, 'should find all users', async (adminInfo, next) => {
    try {
      const { body } = await rest.get('users', {
        accessToken: adminInfo.accessToken
      });
      const found = find(body.data, (a) => a.email === adminInfo.email);
      expect(found.email).to.be.equal(adminInfo.email);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(admins, 'should find all users but not itself', async (adminInfo, next) => {
    try {
      const query = stringify({
        id:{
          $ne: adminInfo.id
        }
      });

      const { body } = await rest.get(`users?${query}`, {
        accessToken: adminInfo.accessToken
      });
      each(body.data, function(u){
        u.id.should.be.not.equal(adminInfo.id);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(admins, 'should find all admins', async (adminInfo, next) => {
    try {
      const query = stringify({
        permissions: {
          $eq: 'users:find,users:get,users:patch,users:remove,books:create,books:find,books:get,books:update,books:remove'
        }
      });

      const { body } = await rest.get(`users?${query}`, {
        accessToken: adminInfo.accessToken
      });
      const found = find(body.data, (a) => a.email === adminInfo.email);
      expect(found.email).to.be.equal(adminInfo.email);
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(admins, 'should find all admins but not itself', async (adminInfo, next) => {
    try {
      const query = stringify({
        id:{
          $ne: adminInfo.id
        },
        permissions: {
          $eq: 'books:create,books:find,books:get,books:put,books:remove'
        }
      });

      const { body } = await rest.get(`users?${query}`, {
        accessToken: adminInfo.accessToken
      });
      each(body.data, function(u){
        u.id.should.be.not.equal(adminInfo.id);
      });
      next();
    } catch (error) {
      next(error);
    }
  });

  it.each(admins, 'should find all guests', async (adminInfo, next) => {
    try {
      const query = stringify({
        permissions: {
          $eq: 'books:find,books:get,books:patch'
        }
      });

      const { body } = await rest.get(`users?${query}`, {
        accessToken: adminInfo.accessToken
      });
      each(body.data, function(u){
        u.id.should.be.not.equal(adminInfo.id);
      });
      next();
    } catch (error) {
      next(error);
    }
  });
});