import * as feathersAuthentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
import { Forbidden } from '@feathersjs/errors';
import {
  disallow,
  iff,
  isProvider
} from 'feathers-hooks-common';

// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;
import {
  checkPermission,
  checkCreation,
  changePermissions
} from './users.hooks.validate';

const isPermitted = function() {
  return iff(
    (context: any) => !context.params.permitted,
    (context: any) => { throw new Forbidden(`User ${context.params.user.id} cannot ${context.method}`); }
  );
};



export default {
  before: {
    all: [],
    find: [
      authenticate('jwt'),
      hashPassword('password'),
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted()
      )
    ],
    get: [
      authenticate('jwt'),
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted()
      )
    ],
    create: [
      hashPassword('password'),
      iff(
        isProvider('external'),
        checkCreation()
      )
    ],
    update: [
      disallow()
    ],
    patch: [
      authenticate('jwt'),
      hashPassword('password'),
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted(),
        changePermissions()
      ),
    ],
    remove: [
      authenticate('jwt'),
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted()
      )
    ]
  },

  after: {
    all: [ 
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
