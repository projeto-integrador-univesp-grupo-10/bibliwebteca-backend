import { Forbidden } from '@feathersjs/errors';
import { includes, map } from 'lodash';

function checkPermission() {
  return async function(context: any) {
    try{
      const service = context.service.Model.name;
      const method = context.method;
      const action = `${service}:${method}`;
      const permissions = context.params.user.permissions.split(',');
      context.params.permitted = includes(permissions, action);
      return context;
    } catch (error) {
      console.log(error);
      context.error = error;
      return context;
    }
  };
}

function checkCreation () {
  return async function(context: any) {
    // check if user wants to create admin
    // if already has at least one admin, forbidden it
    // only admins can update another user to be admin
    // if not found any user, the first user will be a admin
    if(
      includes(context.data.permissions, 'users:update') ||
      includes(context.data.permissions, 'users:remove') ||
      includes(context.data.permissions, 'books:create') ||
      includes(context.data.permissions, 'books:update') ||
      includes(context.data.permissions, 'books:remove')
    ) {
      const response = await context.app.service('users').find();
      if (response.data.length === 0) {
        return context;
      } else {
        const errors = map(context.data.permissions.split(','), function(p) {
          if (includes(context.data.permissions, p)) {
            return `Permission ${p} must be patched by an admin`;
          }
        });
        throw new Forbidden('User cannot be created', errors);
      }
    } else {
      return context;
    }
  };
}

const changePermissions = function() {
  return async (context: any) => {
    const promises = map(context.data.permissions.split(','), function(p) {
      if (includes(context.params.user.permissions.split(','), p)) {
        return Promise.resolve();
      } else {
        const error = new Forbidden(`User ${context.params.user.id} cannot change permissions`, context.data.params);
        return Promise.reject(error);
      }
    });
    try {
      await Promise.all(promises);
      return context;
    } catch (error) {
      throw error;
    }
  };
};

export { checkPermission, checkCreation, changePermissions };