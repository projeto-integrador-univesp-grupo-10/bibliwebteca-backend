import dayjs from 'dayjs';
import { BadRequest } from '@feathersjs/errors';
import { includes } from 'lodash';

enum Action {
  RESERVE='reserve',
  WITHDRAWN='withdrawn',
  RETURN='return'
}

function checkPermission() {
  return function(context: any) {
    const service = context.service.Model.name;
    const method = context.method;
    const action = `${service}:${method}`;
    const user = context.params.user;
    const permissions = user.permissions.split(',');
    context.params.permitted = includes(permissions, action);
    return context;
  };
}

function checkAction() {
  return function(context: any) {
    if (!context.data.action) {
      throw new BadRequest('Patch needs an action. Use \'reserve\', \'withdrawn\' or \'return\'');
    }
    
    const a: Action = context.data.action;
    if (a !== Action.RESERVE && a !== Action.WITHDRAWN && a !== Action.RETURN) {
      throw new BadRequest('Patch needs a valid action. Use \'reserve\', \'withdrawn\' or \'return\'');
    }
  };
}

function checkReserve() {
  return function(context: any) {
    if (context.data.action === Action.RESERVE) {
      if (context.params.book.userId === null && context.params.book.reservation_date === null) {
        context.data.userId = context.params.user.id;
        context.data.reservation_date = dayjs();
      }

      if(context.params.book.userId !== null && context.params.book.reservation_date !== null) {
        if (context.params.book.userId === context.params.user.id) {
          throw new BadRequest(`Book ${context.id} is reserved`, {
            errors: [
              `User ${context.params.user.id} already reserved book ${context.id}`
            ]
          });
        } else {
          throw new BadRequest(`Book ${context.id} is reserved`, {
            errors: [
              `User ${context.params.user.id} do not belongs the reserved book ${context.id}`,
              `Book ${context.id} belongs to user ${context.params.book.userId}`
            ]
          });
        }
      }
    }
  };
}

function checkWithdrawn() {
  return function(context: any) {
    if (context.data.action === Action.WITHDRAWN) {
      if (context.params.book.userId !== null && context.params.book.reservation_date !== null && context.params.book.withdrawn_date === null) {
        if (context.params.book.userId === context.params.user.id) {
          context.data.reservation_date = null;
          context.data.withdrawn_date = dayjs();
          context.data.return_date = dayjs(context.data.withdrawn_date).add(7, 'days');
        } else {
          throw new BadRequest(`Book ${context.id} cannot be withdrawn`, {
            errors: [
              `User ${context.params.user.id} do not belongs book ${context.id}`,
              `Book ${context.id} belongs to user ${context.params.book.userId}`
            ]
          });
        }
      } else {
        if (context.params.book.userId === context.params.user.id) {
          throw new BadRequest(`Book ${context.id} cannot be withdrawn`, {
            errors: [
              `User ${context.params.user.id} already withdrawned book ${context.id}`
            ]
          });
        } else {
          throw new BadRequest(`Book ${context.id} cannot be withdrawn`, {
            errors: [
              `User ${context.params.user.id} do not belongs book ${context.id}`,
              `User ${context.params.book.userId} withdrawned book ${context.id}`,
            ]
          });
        }
      }
    }
  };
}

function checkReturn() {
  return function(context: any) {
    if (context.data.action === Action.RETURN) {
      if (context.params.book.userId !== null && context.params.book.withdrawn_date !== null && context.params.book.return_date !== null) {
        if (context.params.book.userId === context.params.user.id) {
          context.data.userId = null;
          context.data.withdrawn_date = null;
          context.data.return_date = null;
        } else {
          throw new BadRequest(`Book ${context.id} cannot be returned`, {
            errors: [
              `User ${context.params.user.id} do not belongs book ${context.id}`,
              `Book ${context.id} belongs to user ${context.params.book.userId}`
            ]
          });
        }
      } else {
        throw new BadRequest(`Book ${context.id} cannot be returned`, {
          errors: [
            `Book ${context.id} already returned`
          ]
        });
      }
    }
  };
}

export { checkPermission, checkAction, checkReserve, checkWithdrawn, checkReturn };