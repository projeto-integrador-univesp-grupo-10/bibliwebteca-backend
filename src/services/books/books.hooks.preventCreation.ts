import { BadRequest } from '@feathersjs/errors';
import { map } from 'lodash';

export default function(...params: string[]) {
  return async function(context: any) {
    const promises = map(params, function(p) {
      if (context.data[p]) {
        let action = context.method;
        if (action === 'find') {
          action = 'found';
        }
        if (action === 'get') {
          action = 'caught';
        }
        if (action === 'create') {
          action = 'created';
        }
        if (action === 'update') {
          action = 'updated';
        }
        if (action === 'patch') {
          action = 'patched';
        }
        if (action === 'remove') {
          action = 'removed';
        }
        const error = new BadRequest(`Field ${p} may not be ${action}. (preventCreation)`);
        return Promise.reject(error);
      } else {
        return Promise.resolve(true);
      }
    });
    try {
      await Promise.all(promises);
      return context;
    } catch (error) {
      throw error;
    }
  };
}