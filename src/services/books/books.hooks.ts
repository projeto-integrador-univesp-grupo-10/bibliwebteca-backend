import { each } from 'lodash';
import * as authentication from '@feathersjs/authentication';
import { 
  preventChanges,
  setNow,
  iff,
  isProvider,
} from 'feathers-hooks-common';
import { Forbidden } from '@feathersjs/errors';

import preventCreation from './books.hooks.preventCreation';
import {
  checkPermission,
  checkAction,
  checkReserve,
  checkWithdrawn,
  checkReturn
} from './books.hooks.validate';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

const isPermitted = function() {
  return iff(
    (context: any) => !context.params.permitted,
    (context: any) => { throw new Forbidden(`User ${context.params.user.id} cannot ${context.method}`); }
  );
};

const populateBook = function() {
  return async (context: any) => {
    context.params.book = await context.app.service('books').get(context.id);
    return context;
  };
};

export default {
  before: {
    all: [
      authenticate('jwt'),
    ],
    find: [
      (context: any) => {
        each(Object.keys(context.params.query), function (p: string) {
          const val = context.params.query[p];
          if ( typeof val === 'string') {
            if (context.params.query[p] === 'null') {
              context.params.query[p] = null;
            }
          }
          if ( typeof val === 'object') {
            each(['$eq', '$ne'], function (q: string) {
              const _val = context.params.query[p][q];
              if ( typeof _val === 'string' ) {
                if (_val === 'null') {
                  context.params.query[p][q] = null;
                }
              }
            });
          }
        });
        return context;
      }
    ],
    get: [],
    create: [
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted(),
        preventCreation(
          'userId',
          'reservation_date',
          'withdrawn_date',
          'return_date'
        )
      )
    ],
    update: [
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted(),
        preventCreation(
          'userId',
          'reservation_date',
          'withdrawn_date',
          'return_date'
        ),
        setNow('updatedAt')
      )
    ],
    patch: [
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted(),
        populateBook(),
        // Admins cannot reserve, withdraw and return books
        // Guests can reseval erve, withdraw and return books
        preventChanges(
          true,
          'title',
          'author',
          'publishing_company',
          'isbn',
          'edition',
          'pages',
          'subject'
        ),
        checkAction(),
        checkReserve(),
        checkWithdrawn(),
        checkReturn(),
        setNow('updatedAt')
      )
    ],
    remove: [
      iff(
        isProvider('external'),
        checkPermission(),
        isPermitted()
      )
    ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
