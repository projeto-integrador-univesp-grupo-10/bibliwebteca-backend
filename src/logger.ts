import { createLogger, format, transports } from 'winston';
// Configure the Winston logger. For the complete documentation see https://github.com/winstonjs/winston

// Custom format
const { combine, timestamp, printf } = format;

const serverFormat = printf(function({ level, message, timestamp }) {
  let msg = `[${level.toUpperCase()}] | `;
  msg += `${timestamp} | ${message}`;
  return msg;
});

const logger = createLogger({
  // To see more detailed errors, change this to 'debug'
  level: 'info',
  format: combine(
    timestamp(),
    serverFormat
  ),
  transports: [
    new transports.Console()
  ],
});

export default logger;

