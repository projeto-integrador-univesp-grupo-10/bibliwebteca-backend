import urlencode from 'urlencode';
import { Application } from './declarations';

// @title mysql_config
// @param Application: the application to be configured
// @param any: process.env with configurations
function mysql_config(
  application: Application,
  config: any
){
  let mysql_string = application.get('mysql'); 

  mysql_string = mysql_string.replace('MYSQL_USER', config['MYSQL_USER']);
  mysql_string = mysql_string.replace('MYSQL_PASSWORD', urlencode(config['MYSQL_PASSWORD']));
  const host = config['MYSQL_HOST'] + (config['MYSQL_PORT'] ? `:${config['MYSQL_PORT']}` : '');
  mysql_string = mysql_string.replace('MYSQL_HOST', host);
  mysql_string = mysql_string.replace('MYSQL_DATABASE', process.env['MYSQL_DATABASE']);
  application.set('mysql', mysql_string);
}

// @title authentiaction_config
// @param Application: the application to be configured
// @param any: process.env with configurations
function authentiaction_config(
  application: Application,
  config: any
){
  const auth = application.get('authentication');
  auth.secret = auth.secret.replace('AUTH_SECRET', urlencode(config['AUTH_SECRET']));
  auth.jwtOptions.audience = auth.jwtOptions.audience.replace('AUTH_AUDIENCE', urlencode(config['AUTH_AUDIENCE']));
  auth.jwtOptions.issuer = auth.jwtOptions.issuer.replace('AUTH_ISSUER', urlencode(config['AUTH_ISSUER']));
  application.set('authentication', auth);
}

function admin_credentials(
  application: Application,
  config: any
){
  const admin = application.get('admin');
  admin.email = admin.email.replace('ADMIN_EMAIL', urlencode(config['ADMIN_EMAIL']));
  admin.password = admin.password.replace('ADMIN_PASSWORD', urlencode(config['ADMIN_PASSWORD']));
  application.set('admin', admin);
}

export { mysql_config, authentiaction_config, admin_credentials };