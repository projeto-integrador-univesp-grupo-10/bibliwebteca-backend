// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const books = sequelizeClient.define('books', {

    id: { 
      type: DataTypes.UUID,
      unique: true,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },

    title: {
      type: DataTypes.STRING,
      allowNull: false
    },

    author: {
      type: DataTypes.STRING,
      allowNull: false
    },

    publishing_company: {
      type: DataTypes.STRING,
      allowNull: false
    },

    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    isbn: {
      type: DataTypes.STRING(13),
      allowNull: false,
      unique: true
    },

    edition: {
      type: DataTypes.STRING,
      allowNull: false
    },
    
    pages: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    subject: {
      type: DataTypes.STRING,
      allowNull: false
    },

    withdrawn_date: {
      type: DataTypes.DATE,
      allowNull: true
    },

    return_date: {
      type: DataTypes.DATE,
      allowNull: true
    },

    reservation_date: {
      type: DataTypes.DATE,
      allowNull: true
    },

  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (books as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    const { users } = models;
    books.belongsTo(users);
  };

  return books;
}