// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';
import { map, flatten, includes } from 'lodash';

const PERMISSIONS = flatten(map(['users', 'books'], function(service) {
  let actions = null;
  if (service === 'users') {
    actions = ['find', 'get', 'update', 'patch', 'remove'];
  } else {
    actions = ['create', 'find', 'get', 'update', 'patch', 'remove'];
  }
  return map(actions, function(method) {
    return `${service}:${method}`;
  });
}));

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    
    id: { 
      type: DataTypes.UUID,
      unique: true,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    permissions: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'users:get,users:patch,books:find,books:get,books:patch',
      validate: {
        async checkPermissions (permissions: string) {
          const promises = map(permissions.split(','), function(p) {
            if (includes(PERMISSIONS, p)) {
              return Promise.resolve();
            } else {
              const error = new Error(`Permission ${p} not valid`);
              return Promise.reject(error);
            }
          });
          try {
            await Promise.all(promises);
          } catch (error) {
            throw error;
          }
        }
      }
    }
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (users as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    const { books } = models;
    users.hasOne(books);
  };

  return users;
}
