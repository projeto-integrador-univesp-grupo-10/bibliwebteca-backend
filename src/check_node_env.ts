//loads custom environment
import { config } from 'dotenv';
import fs from 'fs';
import { each } from 'lodash';

export default function() {
  if (!process.env['NODE_ENV']) {
    process.env['NODE_ENV'] = 'development';
  }

  const node_env = process.env['NODE_ENV'];
  if (node_env !== 'development' &&
    node_env !== 'test' &&
    node_env !== 'test-ci' &&
    node_env !== 'staging' &&
    node_env !== 'deploy' &&
    node_env !== 'production'
  ) {
    throw new Error(`Unknow NODE_ENV=${node_env}. Use \'development\', \'test\', \'test-ci\', \'staging\', \'deploy\' or \'production\'`);
  }

  if (node_env === 'development' || node_env === 'test') {
    if (fs.existsSync('.env')){
      config();
    } else {
      throw new Error(`In ${node_env} mode you must assign a .env file`);
    }
  }

  // check variables
  each([
    'MYSQL_USER',
    'MYSQL_PASSWORD',
    'MYSQL_HOST',
    'MYSQL_PORT',
    'MYSQL_DATABASE',
    'AUTH_SECRET',
    'AUTH_AUDIENCE',
    'AUTH_ISSUER'
  ], function(v) {
    if (!process.env[v]) {
      throw new Error(`Environment variable \'${v}\' not defined`);
    }
  });

  each([
    'ADMIN_EMAIL',
    'ADMIN_PASSWORD'
  ], function(v) {
    if ((node_env === 'development' || node_env === 'test' || node_env === 'staging') && !process.env[v]) {
      throw new Error(`Environment variable \'${v}\' not defined`);
    }
  });
}