import { map, update } from 'lodash';

/**
 * function eager_loading
 * 
 * Use it Application find hook that run for every service. 
 * It will add sequelize parameter
 * to eager loading any related models when the keyword 'include'
 * is on query.
 *
 * @returns function
 */
export default function() {
  return async function (context: any) {
    if (context.params.query && context.params.query.include) {
      context.params.sequelize = {
        include: map(context.params.query.include, function(query) {
          return update(query, 'model', function(m) {
            return context.app.service(m).Model;
          });
        })
      };
      delete context.params.query.include;
    }
    return context;
  };
}