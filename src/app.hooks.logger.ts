import { template, map, each, TemplateExecutor } from 'lodash';
import logger from './logger';

type MessagePromise = Promise<void>
type MessagePromises = Array<MessagePromise>

function starEmailSubstitution (v: string, i: number): string {
  if (i !== 0) {
    return '*';
  } else {
    return v;
  }
}

function obfuscateMailAddress(email: string): string {
  const compiled: TemplateExecutor = template('${ nick }@${ addr }');
  const params: string[] = email.split('@');
  const nick: string = map(params[0], starEmailSubstitution).join('');
  const addr: string = map(params[1], starEmailSubstitution).join('');
  return compiled({ nick: nick, addr: addr});
}

export default function(options: any){
  return async function(context: any): Promise<any>{
    const promises:MessagePromises = map(options, function(value: string, key: string){
      return new Promise(function(resolve){

        if (process.env['NODE_ENV'] === key && value !== 'none') {

          let message = '';

          if (context.params.headers && context.params.headers['user-agent']) {
            message += context.params.headers['user-agent']+' | ';
          } else {
            message += 'internal call | ';
          }
          
          if (context.method === 'find' || context.method === 'get'){
            message += 'GET';
          }

          if (context.method === 'create') {
            message += 'POST';
          }
          
          if (context.method === 'update') {
            message += 'UPDATE';
          }

          if (context.method === 'patch') {
            message += 'PATCH';
          }

          if (context.method === 'remove') {
            message += 'DELETE';
          }

          if (context.path) {
            message += ` /${context.path}`;
          }

          if (context.id) {
            message += `/${context.id}`;
          }

          if (context.params.query && Object.keys(context.params.query).length > 0) {
            message += '?';
            message += map(context.params.query, function(val, key) {
              if (key === 'email') {
                const newVal = obfuscateMailAddress(val);
                return `${key}=${newVal}`;
              } else {
                if (typeof val === 'object') {
                  val = map(val, (v, k) => `[${k}]=${v}`).join('&');
                  return `${key}=${val}`;
                } else {
                  return `${key}=${val}`;
                }
              }
            }).join('&');
          }

          if (context.data) {
            const obj: any = {};
            each(context.data, function(val, key) {
              if (key === 'email') {
                obj.email= obfuscateMailAddress(val);
              } else if (key !== 'password') {
                obj[key] = val;
              }
            });
            message += ` | data: ${JSON.stringify(obj)}`;
          }
          if (value === 'info') {
            logger.info(message);
          } 

          if (value === 'warn') {
            logger.warn(message);
          }

          if (value === 'error' && key !== 'test') {
            logger.error(message);
          }
        }
        resolve();
      });
      
    });
    try {
      await Promise.all<MessagePromises>(promises);
      return context;
    } catch (error){
      logger.error(error);
    }
  };
}
