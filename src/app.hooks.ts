// Application hooks that run for every service
// Don't remove this comment. It's needed to format import lines nicely.
import applogger from './app.hooks.logger';
import eager_loading from './eager_loading';

export default {
  before: {
    all: [
      applogger({
        development: 'info',
        test: 'none',
        staging: 'none',
        deploy: 'info',
        production: 'info'
      })
    ],
    find: [
      eager_loading()
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      applogger({
        development: 'error',
        test: 'error',
        staging: 'error',
        deploy: 'error',
        production: 'error'
      })
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
