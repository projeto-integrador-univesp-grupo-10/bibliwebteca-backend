import logger from './logger';
import app from './app';

const port = app.get('port');
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) => {
  logger.error('Unhandled Rejection at: Promise ', p, reason);
  p.catch((error) => {
    logger.error(error);
    console.log(reason);
  });
});

server.on('listening', () => {
  const msg = `${app.get('host')}:${app.get('port')}`;
  logger.info(`Feathers application started on ${msg}`);
});
